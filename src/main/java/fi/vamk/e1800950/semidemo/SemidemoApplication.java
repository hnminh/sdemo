package fi.vamk.e1800950.semidemo;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SemidemoApplication {

	@Autowired
	private AttendanceRepository repository;
	public static void main(String[] args) {
		SpringApplication.run(SemidemoApplication.class, args);
	}

	@Bean
	public void initDate(){
		Attendance att = new Attendance("QWERTY", new Date(2020, 10, 20));
		Attendance att2 = new Attendance("ABC", new Date(2020, 10, 20));
		Attendance att3 = new Attendance("XYZ", new Date(2020, 10, 20));
		repository.save(att);
		repository.save(att2);
		repository.save(att3);
	}
}
