package fi.vamk.e1800950.semidemo;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
class Controller {
    @GetMapping("/")
    public String swagger() {
        return "redirect:/swagger-ui.html";
    }

    @RequestMapping("/test")
    public String test(){
        return "{\"id\":1}";
    }
}