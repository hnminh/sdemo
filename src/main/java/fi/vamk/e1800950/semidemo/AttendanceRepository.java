package fi.vamk.e1800950.semidemo;

import org.springframework.data.repository.CrudRepository;
import java.util.Date;

public interface AttendanceRepository extends CrudRepository<Attendance, Integer>{
    public Attendance findByKey(String key);
    public Attendance findByDate(Date date);
}