package fi.vamk.e1800950.semidemo;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.security.test.context.support.WithMockUser;

@SpringBootTest
@AutoConfigureMockMvc
public class AttendanceControllerTests extends AbstractTest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private AttendanceRepository repository;

    @WithMockUser("USER")
    @Test
    public void getList() throws Exception {
        // Lets create an item, the database will retun the object
        // with given id and the three object made by Bean in
        Attendance at = new Attendance("XYZT");
        at = repository.save(at);
        // the database returns the object inside an array, let create similar
        Attendance[] arr = new Attendance[1];
        arr[0] = at;
        MvcResult mvcResult = mvc.perform(
                MockMvcRequestBuilders.get("/attendance/" + at.getId()).accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(super.mapToJson(at), content);
    }

    @WithMockUser("USER")
    @Test
    public void createAttendanceViaPost() throws Exception {
        Attendance att = new Attendance("ABCDE");
        String inputJson = super.mapToJson(att);
        // The Bean already inserted three objects and previous test fouthh one
        // so expecting to get id =5
        att.setId(5);
        System.out.println(inputJson);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/attendance")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(super.mapToJson(att), content);
    }
}
